# Trainer

[<img src="../images/Meirian.jpg" alt="Meirian Lovelace-Tozer. Photo credit: Eric Jong. (2019)." title="Meirian Lovelace-Tozer. Photo credit: Eric Jong. (2019)." width="256" height="256">](../images/Meirian.jpg)

Hi, I'm Meirian.

I am a teacher, consultant and senior community co-ordinator at the University of Melbourne. I offer training in Ubuntu and LaTeX at Research Computing Services. My other areas of interest include data analysis, operations research and statistics, which I also have the pleasure of teaching at the University. I love teaching and am enthusiastic about enabling others to benefit from the skills that I teach! 

Get in touch with me for more information about Ubuntu training, events, and community support:

* Twitter: Follow or tweet at me on [@MeirianLT](www.twitter.com/MeirianLT) and [@ResCom_unimelb](https://twitter.com/ResCom_unimelb).
* YouTube: Watch some of our videos and subscribe to the [ResCom Team](https://www.youtube.com/channel/UCGWU-ESE1j2vCJTACZHHLGQ)!
* Instagram: Check out our photos and gifs on the [ResCom_unimelb Instagram](https://www.instagram.com/rescom_unimelb).
* Email: Get in contact with me at: mlovelace(at)unimelb.edu.au  
(Replace the "(at)" with the "@" symbol).

I look forward to meeting you at our next training!

---

Continue to [Training format](../training-format/README.md), or (re)visit one of the other pages listed [here](../SUMMARY.md).

